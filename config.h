/* user and group to drop privileges to */
static const char *user  = "projjalm";
static const char *group = "projjalm";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "black",     /* after initialization */
	[INPUT] =  "#004422",   /* during input */
	[FAILED] = "#442200",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;

/* time in seconds before the monitor shuts down */
static const int monitortime = 10;
